<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Models\User;


class ListController extends Controller
{

    public function list(Request $request)
    {

        $users = DB::table('user')
                    ->select('user.id', 'user.name', 'user.email', 'user.password', 'user.created_at');


        return response()->json(User::all(),200);
    }


}
